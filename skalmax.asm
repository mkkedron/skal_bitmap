
section .data

section .text
global skalmax


skalmax:

        push rbp
        mov rbp, rsp
       ; sub rsp, 56     ;robienie miejsca na stosie

        mov qword [rbp-8], rdi 	; w
        mov qword [rbp-16],rsi       ; h
        mov qword [rbp-24], rdx       ;obraz1
        mov qword [rbp-32], rcx      ;obraz2
        movsd qword [rbp-40], xmm0     ; ratio
        
        %idefine w [rbp-8]    
        %idefine h [rbp-16]        
        %idefine obraz1 [rbp-24]             
        %idefine obraz2 [rbp-32] 
        %idefine ratio [rbp-40]  
	;%idefine tmp [rbp-]
	;%idefine tmp2 [rbp-56]         
        
   
main_loop:
	xor	r8,r8		;r8-licznik wierszy
	mov r10,obraz1		;ptr source
	mov r15,obraz2		;ptr destiny
h_loop:

	xor	r9,r9	
	cvtsi2sd xmm3 ,r8	;r13=(int)xmm3=(float)i/(float)ratio -z jakiego wiersza pobieramy pixele
	
	movsd xmm4,xmm0
	divsd xmm3,xmm4
	xor r13,r13
	cvttsd2si r13,xmm3	;i/ratio	liczymy raz na cala linie
	mov rax,h
	;cmp r13,rax
	;jl next
	;mov r13,rax
next:
	;mov r10,obraz1
	mov r13,[r10+8*r13]		;ptr source
next2:
	;mov r15,obraz2
	;xor r14,r14
	mov r14,[r15+8*r8]		;ptr destiny
	

w_loop:
	cvtsi2sd xmm1,r9	
	movsd	 xmm2, xmm0
	divsd xmm1,xmm2 	;j/ratio

	cvttsd2si r12,xmm1	;j/ratio	r12= ktory pixel z wiersza pobieramy?
	xor rax,rax
	

	mov rax,3
	mul r12
	mov r12,rax  	;mnozymy r12*3 (3 bity na pixel)


	add r12 , r13		;dodajemy adres poczatku pixela w wierszu do wskaznika linii
	
	xor r11,r11
	movzx r11d, byte [r12]	;do r11d bajt , r10=obraz1
	mov  byte [r14], r11b	;
	add r12,1
	add r14,1
	movzx r11d, byte [r12]
	mov byte [r14],r11b
	add r12,1
	add r14,1
	movzx r11d, byte [r12]
	mov byte [r14],r11b
	add r14,1
	xor r12,r12		;czyscimy (tak na wszelki wypadek ;p)
	mov rax,w	
	add r9,1		;inc(j)
	cmp r9,rax		;czy to koniec wiersza?
	jl w_loop



	add r8,1		;inc(i)
	
	mov rax, h
	cmp r8,rax		;czy to koniec pliku?
	jl h_loop
	


	
  
;  end_main_loop:


	mov			rsp, rbp
	pop			rbp
        
  ret   ;koniec funkcji
