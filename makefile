CC = gcc
OBJ = skal.o skalmax.o
LINKOBJ = skal.o skalmax.o
BIN = skal
CFLAGS = -Wall 
$(BIN): $(OBJ)
	$(CC) $(LINKOBJ) $(CFLAGS) -o $(BIN) `pkg-config --cflags --libs allegro`
main.o: skal.c
	$(CC) $(CFLAGS) -c skal.c -o skal.o
skalmax.o: skalmax.asm
	yasm -f elf64 skalmax.asm
	
del:
	rm -f skal.o skal skalmax.o
