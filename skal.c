/*
 * morph.c
 * 
 * Copyright 2013 Maciej Kędroń <mkkedron@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <allegro.h>
#include<assert.h>



void skalmax(int imgw,int imgh, unsigned char ** source_line, unsigned char ** copy_line,double ratio);

int main(int argc, char **argv)
{
	if (argc != 2) {
      allegro_message("Usage: 'skal yourbitmap.bmp'\n");
      return 1;
	}	
	double ratio;
	allegro_init();
	install_keyboard();
	set_window_title("Skal App.");			
	BITMAP *source_image;	
	PALETTE the_palette;		
		
	source_image = load_bitmap(argv[1], the_palette);				
	int imgw = source_image->w;
	int imgh = source_image->h;		
	destroy_bitmap(source_image);	
	
	 int screenw = imgw; 
	 int screenh = imgh;
	 
    set_color_depth(24);
set_gfx_mode(GFX_AUTODETECT_WINDOWED, screenw, screenh, 0, 0);	
			
    
	source_image = load_bitmap(argv[1], the_palette);				
	
	BITMAP *copy_image = create_bitmap(imgw, imgh);
	blit(source_image, copy_image, 0, 0, 0, 0, imgw, imgh);					
			
	blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	
	blit(source_image, screen, 0, 0, imgw, 0, imgw, imgh);	
	
	while (1)
	{
		if (key[KEY_LEFT])
		{
			{			
				if(imgw>source_image->w) {
imgw-=50; 
imgh-=50;

ratio=((float)imgw)/((float)screenw);		
set_gfx_mode(GFX_AUTODETECT_WINDOWED, imgw, imgh, 0, 0);	
				destroy_bitmap(copy_image);
				copy_image= create_bitmap(imgw,imgh);	
set_gfx_mode(GFX_AUTODETECT_WINDOWED, imgw, imgh, 0, 0);
				skalmax(imgw,imgh,source_image->line, copy_image->line,ratio);						
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);
}	
			}
readkey();
		}
		
		if (key[KEY_RIGHT])
		{			
			{
				imgw+=50;
				imgh+=50;
				ratio=(((float)imgw)/((float)screenw));
				destroy_bitmap(copy_image);
				copy_image= create_bitmap(imgw,imgh);
set_color_depth(24);
set_gfx_mode(GFX_AUTODETECT_WINDOWED, imgw, imgh, 0, 0);	
				skalmax(imgw,imgh, source_image->line, copy_image->line,ratio);	
//assert(0);		
set_color_depth(24);
				blit(copy_image, screen, 0, 0, 0, 0, imgw, imgh);	//assert(0); 
readkey();
			}
		}
		
		if (key[KEY_ESC])
		{
			break;
		}		
	}
				
	destroy_bitmap(source_image);	
	destroy_bitmap(copy_image);	
	 readkey();			
		
	return 0;
}
END_OF_MAIN();

/*
ratio- stosunek nowego rozmiaru pliku do pierwotnego (ratio=copy_bitmap->w / screenw)
przechodzimy kazdy pixel nowego pliku zznajdujac mu odpowiedniki pixeli w starym pliku (   nowy_pixel[i][j]   =  stary_pixel[i/ratio][j/ratio]  )

void skalmax(int imgw,int imgh,unsigned char** source, unsigned char** final,float ratio)
{
	//while(1)
		{
		int i=1,j=0,a=0,b=0,tmp=0;
		for(i=0;i<imgh;++i)
		{
		for (j=0;j<imgw;j++)
		{
			tmp=((int)(((float)j)/ratio));
			tmp*=3;
			final[i][3*j]=source[(int)(i/ratio)][(int)(tmp)];
//assert(200-i);
			final[i][3*j+1]=source[(int)(i/ratio)][(int)(tmp+1)];
			final[i][3*j+2]=source[(int)(i/ratio)][(int)(tmp+2)];
					
		}

		}

		}
//assert(0);
return ;

}
*/

